package com.devcamp.provincedistrictward.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.provincedistrictward.model.District;

public interface IDistrictRepository extends JpaRepository<District, Integer> {
  District findById(int Id);
}
