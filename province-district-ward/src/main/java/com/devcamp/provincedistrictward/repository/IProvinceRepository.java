package com.devcamp.provincedistrictward.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.provincedistrictward.model.Province;

public interface IProvinceRepository extends JpaRepository<Province, Integer> {
  Province findById(int Id);
}
