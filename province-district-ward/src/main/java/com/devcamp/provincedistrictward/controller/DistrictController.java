package com.devcamp.provincedistrictward.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.provincedistrictward.model.District;
import com.devcamp.provincedistrictward.model.Ward;
import com.devcamp.provincedistrictward.service.DistrictService;
import com.devcamp.provincedistrictward.service.WardService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class DistrictController {
  @Autowired
  DistrictService districtService;

  @Autowired
  WardService wardService;

  @GetMapping("/devcamp-alldistricts")
  public ResponseEntity<List<District>> getAllDistricts() {
    try {
      return new ResponseEntity<>(districtService.getAllDistricts(),
          HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
  }

  @GetMapping("/devcamp-wards")
  public ResponseEntity<Set<Ward>> getWardByDistrictId(@RequestParam(value = "id") int id) {
    try {
      Set<Ward> vWards = districtService.getWardByDistrictId(id);
      if (vWards != null) {
        return new ResponseEntity<>(vWards, HttpStatus.OK);
      } else {
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
      }
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }
}
