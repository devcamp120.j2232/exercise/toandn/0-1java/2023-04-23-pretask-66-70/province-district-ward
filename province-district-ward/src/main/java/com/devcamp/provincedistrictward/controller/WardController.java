package com.devcamp.provincedistrictward.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.provincedistrictward.model.Ward;
import com.devcamp.provincedistrictward.service.WardService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class WardController {
  @Autowired
  WardService wardService;

  @GetMapping("/devcamp-allwards")
  public ResponseEntity<List<Ward>> getAllWards() {
    try {
      return new ResponseEntity<>(wardService.getAllWards(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
  }
}
