package com.devcamp.provincedistrictward.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "wards")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Ward {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;

  @Column(name = "name_ward")
  private String nameWard;

  @Column(name = "prefix_ward")
  private String prefixWard;

  @ManyToOne
  @JsonIgnore
  @JoinColumn(name = "district_id")
  private District district;

  public Ward() {
  }

  public Ward(int id, String nameWard, String prefixWard, District district) {
    this.id = id;
    this.nameWard = nameWard;
    this.prefixWard = prefixWard;
    this.district = district;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getNameWard() {
    return nameWard;
  }

  public void setNameWard(String nameWard) {
    this.nameWard = nameWard;
  }

  public String getPrefixWard() {
    return prefixWard;
  }

  public void setPrefixWard(String prefixWard) {
    this.prefixWard = prefixWard;
  }

  public District getDistrict() {
    return district;
  }

  public void setDistrict(District district) {
    this.district = district;
  }
}
